# UMLDesignTool

## Describe

this project is making a webpage-based UML class diagram design tool.

## Goal

1. ClassBox
   1. Classbox's body ----(done) ----(fixed:modify title, delete class button)
      1. Title
         1. TitleState:
            1. titleName:string
            2. editingTitle:string
            3. mode:string('init','modifing','normal')
         2. make normal mode
            1. make title
            2. make delete button
      2. Attrbutes
         1. Attribute item can be created, deleted, modified
         2. Click add button to add Attribute
         3. Click delete button to delete item
         4. Double click item to modify item
      3. Operations
         1. Operation item can be created, deleted, modified
         2. Click add button to add Operation
         3. Click delete button to delete item
         4. Double click item to modify item
      4. Responsibilities
         1. Responsibility can be modify
   2. Global ----(done)
      1. Classbox can be move(by drag)
      2. Classbox can be created, deleted
      3. Classbox create by create button
      4. Classbox delete by delete button at classbox's title
   3. Relationship line ----(done)
      1. Relationship line can be created, deleted, modified
      2. Relationship line can be moved by classbox
      3. Relationship line will be deleted when classbox is deleted

## Step

1. Make classbox ----(done)
   1. Make classbox shell ----(done)
   2. Make titleBar ----(done) ----(fixit:modify,delete)
      1. make modify function ----(done)
      2. make delete function
   3. Make AttributesBox ----(done)
      1. Make box shell ----(done)
      2. Make AttrNode class ----(done)
         1. fields ----(done)
            1. Name:String
               1. default:"AttributeName"
            2. Accessibility:String ----(done)
               1. "Private"
               2. default:"Public"
               3. "Protected"
            3. TypeName:String ----(done)
               1. Default:"Any"
            4. Status:String ----(done)
               1. in constructor: "init".
               2. "normal"
               3. "modifing"
         2. constructor(name,typename,accessibility) ----(done)
         3. getName() ----(done)
         4. setName() ----(done)
         5. getAccessibility() ----(done)
         6. setAccessibility() ----(done)
         7. getTypeName() ----(done)
         8. setTypeName() ----(done)
      3. Make add item function ----(done)
      4. make delete item function ----(done)
      5. make replace item function ----(done)
      6. make item switch function ----(done)
      7. make render ----(done)
         1. make attrbox render ----(done)
            1. make attrbox titlebar ----(done)
            2. make attritem render ----(done)
               1. modify mode ----(done)
               2. normal mode ----(done)
      8. make item modify function
         1. Add button click function ----(done)
         2. Delete button click function ----(done)
            1. Make attritem selectable ----(done)
               1. make lost focus function ----(done)
         3. modify submit button click funcion ----(done)
         4. normal to modify(double click) ----(done)
      9. make scale function ----(todo)
   4. Make OperationBox
      1. Copy code from attribute box ----(done)
      2. Modify the code ----(done)
   5. Make ResponsibilityBox ----(jump)
      1. Make box shell
      2. Make describe function
2. Classbox global operation
   1. Make classbox dragable ----(done)
      1. Add coordinate of box ----(done)
      2. Move ClassBox from app.js to global.js ----(done)
      3. Make classbox store class ----(done)
         Use Map to store ClassBox
         1. addClassBox ----(done)
         2. deleteClassBox ----(done)
         3. findClassBoxByKey ----(done)
         4. getAllClassBox ----(done)
   2. Make classbox focusable ----(done)
   3. Make classbox add function ----(done)
   4. Make classbox delete function ----(done)
3. RelationShipSign
   1. Add RelationshipCreation mode in Classbox ----(done)
      1. Block mouse event when in RelationshipCreation mode ----(done)
      2. make 4 div surround div to detect coordinate ----(done)
      3. make notice from WorkSpace.js ----(done)
   2. Make Classbox edge cordinate calculate function ----(done)
   3. Make relationship store class ----(done)
      1. fields
         1. owner
         2. container
         3. counter
      2. addRelationship(relationStart,relationEnd)
      3. deleteRelationship(RelationshipKey)
      4. removeInvalidRelationship(deletedClassKey)
   4. Draw relationship line ----(done)
      1. make the line
         1. use 0px height div ----(done)
         2. make line move when box move ----(done)
         3. put to correct location ----(done)
         4. double click to edit ----(done)
      2. receive notice from class when class resize or move ----(done)
      3. make sign ----(done)
         1. Association
         2. Inheritance
         3. Implementation
         4. Dependency
         5. Aggregation
         6. Composition
   5. Let relationship line can be deleted ----(done)
   6. Let relationship line be deleted when classbox delete ----(done)
   7. Refactory
      1. separate control from react component(attrbox, operationbox)
