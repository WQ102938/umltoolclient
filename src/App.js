import React from 'react';
import WorkSpace from './Components/WorkSpace/WorkSpace'
import MenuBar from './Components/MenuBar/MenuBar'

class Global extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state = {
      AddClassBox: { status: false, coordinateX: null, coordinateY: null },
      mode: 'normal',
    }
  }

  render()
  {
    return (
      <div className="App">
        <MenuBar noticeGlobal={this.menuCallBack.bind(this)} globalState={this.state}></MenuBar>
        <WorkSpace noticeGlobal={this.workSpaceCallBack.bind(this)} globalState={this.state}></WorkSpace>
      </div>
    )
  }

  menuCallBack(command, param)
  {
    switch (command)
    {
      default: return;
      case 'AddClassBox': { this.setState({ AddClassBox: { status: true, coordinateX: param.coordinateX, coordinateY: param.coordinateY } }); break; }
      case 'SetRelationshipEditingMode': { param.mode ? this.setState({ mode: 'RelationshipEditing' }) : this.setState({ mode: 'normal' }); break; }
    }
  }

  workSpaceCallBack(command, param)
  {
    switch (command)
    {
      default: return;
      case 'ResetAddClassBox': { this.setState({ AddClassBox: { status: false, coordinateX: null, coordinateY: null } }); break; }
    }
  }
}

function App()
{
  return (
    <Global></Global>
  );
}

export default App;
