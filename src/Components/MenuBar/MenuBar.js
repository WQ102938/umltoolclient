import React from 'react'
import styled from 'styled-components'

export default class MenuBar extends React.Component
{

    constructor(props)
    {
        super(props);
        this.state = {

        }

        this.ShellStyle = styled.div`
        position:absolute;
        width:100%;
        height:30px;
        box-shadow:1px 1px black inset,-1px -1px black inset;
        display:flex;
        align-items:center;
        `

        this.addClassBoxButton = styled.button`
        width:auto;
        height:20px;
        margin:5px 5px 5px 5px;
        `

        this.modeSwitchCheckbox = styled.input`
        width:auto;
        padding:none;
        height:15px;
        width:15px;
        margin:5px 5px 5px 5px;
        `
    }

    render()
    {
        return (
            <this.ShellStyle>
                <this.addClassBoxButton onClick={this.addClassBox.bind(this)}>Add class box</this.addClassBoxButton>
                <this.modeSwitchCheckbox type='checkbox' onClick={(e) => { this.setRelationshipEditingMode(e.target.checked) }}></this.modeSwitchCheckbox>
                <div>RelationshipEditingMode</div>
            </this.ShellStyle>
        )
    }

    addClassBox()
    {
        this.props.noticeGlobal('AddClassBox', { 'coordinateX': 10, 'coordinateY': 10 });
    }

    setRelationshipEditingMode(mode)
    {
        this.props.noticeGlobal('SetRelationshipEditingMode', { 'mode': mode });
    }
}