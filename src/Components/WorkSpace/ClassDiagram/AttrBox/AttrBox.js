import React from 'react'
import ReactDom from 'react-dom'
import styled from 'styled-components'
import AttrTitleBar from './AttrTitleBar/AttrTitleBar'
import AttrItem from './AttrItem/AttrItem'

export default class ClassAttrBox extends React.Component
{
    constructor(props)
    {
        super(props);

        this.attrContainer = new AttrContainer(this);

        this.AttrBoxShell = styled.div`
        position:relative;
        width:100%;
        height:${(props) => { return (props.state.classBoxState.LineHeight * (1 + props.length * 0.75)).toString().concat('px') }};
        left:0px;
        box-shadow:1px 1px black inset,-1px -1px black inset;
        `

        this.state = {
            classBoxState: this.props.state,
            selectedAttrItem: -1,
        }

        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState)
    {
        if (nextState !== this.state)
        {
            return true;
        } else
        {
            return false;
        }
    }

    componentWillUpdate()
    {
        this.props.updateClassBoxHeight(this.attrContainer.getSize() * 0.75 + 1);
    }

    render()
    {
        return (
            <this.AttrBoxShell state={this.state} length={this.attrContainer.getSize()}>
                <AttrTitleBar
                    state={this.state}
                    addItem={this.attrContainer.addItem.bind(this.attrContainer)}
                    deleteItem={this.attrContainer.deleteItem.bind(this.attrContainer)} >
                </AttrTitleBar>
                {this.attrContainer.getAllItems().map((value, index) =>
                {
                    return <AttrItem
                        key={value.getId()}
                        nodeInfo={value}
                        state={this.state}
                        id={value.getId()}
                        setFocus={this.getSelectedAttrItem.bind(this)}
                        updateItem={this.updateItem.bind(this)}>
                    </AttrItem>
                }, this)}
            </this.AttrBoxShell>
        )
    }

    // Item operations

    updateItem(id, name, type, accessibility, status)
    {
        this.attrContainer.updateItem(id, (item) =>
        {
            if (status !== 'modifing')
            {
                item.setName(name);
                item.setAccessibility(accessibility);
                item.setTypeName(type);
            }
            item.setStatus(status);
        })
    }

    // callback function for child select setting
    getSelectedAttrItem(id)
    {
        this.setState({ selectedAttrItem: id });
    }

    //detect outside click;
    componentDidMount()
    {
        document.addEventListener('mousedown', this.handleClickOutside, true);
    }

    componentWillUnmount()
    {
        document.removeEventListener('mousedown', this.handleClickOutside, true);
    }

    handleClickOutside(event)
    {
        let domNode = ReactDom.findDOMNode(this);
        if (!domNode || !domNode.contains(event.target))
        {
            this.setState({ selectedAttrItem: -1 });
        }
    }
}

class AttrContainer
{
    constructor(owner)
    {
        this.container = new Map();
        this.counter = 0;
        this.owner = owner;
    }

    addItem()
    {
        this.container.set(this.counter, new AttrNode(this.counter));
        this.counter++;
        this.owner.forceUpdate();
    }

    deleteItem(id)
    {
        this.container.delete(id);
        this.owner.forceUpdate();
    }

    getItem(id)
    {
        if (this.container.has(id))
        {
            return this.container.get(id);
        } else
        {
            console.log('id not found in AttrContainer');
            return null;
        }
    }

    getAllItems()
    {
        let tmp = [];
        for (let value of this.container.values())
        {
            tmp.push(value);
        }
        return tmp;
    }

    updateItem(id, updateMethod)
    {
        let tmp = this.getItem(id);
        let result = updateMethod(tmp);
        if (typeof (result) !== 'undefined')
            this.container.set(id, result);
        this.owner.forceUpdate();
    }

    getSize()
    {
        return this.container.size;
    }
}

class AttrNode
{
    constructor(id)
    {
        this.id = id;
        this.typename = null;
        this.name = null;
        this.accessibility = null;
        this.status = "init";
        this.setName(null);
        this.setAccessibility(null);
        this.setTypeName(null);
    }

    getId() { return this.id }

    getName() { return this.name }

    setName(name)
    {
        if (name == null || typeof name != 'string')
        {
            this.name = "AttrName";
        } else
        {
            this.name = name;
        }
    }

    getAccessibility() { return this.accessibility }

    setAccessibility(accessibility)
    {
        switch (accessibility)
        {
            case "Public": { this.accessibility = accessibility; break; }
            case "Private": { this.accessibility = accessibility; break; }
            case "Protected": { this.accessibility = accessibility; break; }
            default: this.accessibility = "Public"; break;
        }
    }

    getTypeName() { return this.typename }

    setTypeName(typename)
    {
        if (typename == null || typeof (typename) != "string")
        {
            this.typename = "any";
        } else
        {
            this.typename = typename;
        }
    }

    setStatus(status)
    {
        if (typeof (status) !== 'string')
        {
            this.status = "normal";
        } else
        {
            this.status = status;
        }
    }
}