import React from 'react'
import styled from 'styled-components'

export default class AttrTitleBar extends React.Component
{

    constructor(props)
    {
        super(props);
        this.state = {
            attrBoxState: this.props.state,
        }
        this.TitleBar = styled.div`
        position:relative;
        top:0px;
        left:0px;
        width:100%;
        height:${props => { return props.state.attrBoxState.classBoxState.LineHeight.toString().concat('px') }};
        display:flex;
        user-select:none;
        `
        this.AttrStr = styled.p`
        margin:0px;
        top:0px;
        left:0px;
        height:100%;
        font-size:${props => { return (props.state.attrBoxState.classBoxState.LineHeight / 2).toString().concat('px') }};
        display:flex;
        justify-content:left;
        align-items:center;
        padding-left:10px;
        margin-right:auto;
        user-select:none;
        `

        this.AttrAddButton = styled.button`
        margin:4px 4px 4px 4px ;
        width:${props => { return (props.state.attrBoxState.classBoxState.LineHeight-8).toString().concat('px') }};
        user-select:none;
        padding:0px;
        `

        this.AttrDeleteButton = styled.button`
        margin:4px 4px 4px 4px ;
        user-select:none;
        width:${props => { return (props.state.attrBoxState.classBoxState.LineHeight-8).toString().concat('px') }};
        `

    }

    render()
    {
        return (
            <this.TitleBar state={this.state}>
                <this.AttrStr state={this.state}>Attributes</this.AttrStr>
                <this.AttrAddButton state={this.state} onClick={this.props.addItem}>+</this.AttrAddButton>
                <this.AttrDeleteButton state={this.state} onClick={(e) => { this.props.deleteItem(this.props.state.selectedAttrItem) }}>-</this.AttrDeleteButton>
            </this.TitleBar>
        )
    }

}