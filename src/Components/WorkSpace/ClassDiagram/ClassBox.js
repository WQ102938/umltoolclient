import React from 'react'
import ReactDom from 'react-dom'
import styled from 'styled-components'
import ClassAttrBox from './AttrBox/AttrBox'
import ClassOperationBox from './OperationBox/OperationBox'
import ClassTitleBar from './TitleBar/Title'

export default class ClassBox extends React.Component
{

    constructor(props)
    {
        super(props);
        this.state = {
            TitleName: "Testg文字テスト",
            AttrQuantity: 1,
            OperationQuantity: 1,
            ResponsibilityQuantity: 0,
            LineHeight: 30,
            BoxWidth: 300,
            Selected: false,
            MakeRelationshipMode: false,
            RelationshipBoxInfo: { location: null, x: null, y: null }
        }

        this.Coordinate = { x: this.props.coordinateX, y: this.props.coordinateY };
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.drag = { status: false, prevX: null, prevY: null }
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDragging = this.handleDragging.bind(this);
        this.handleDragEnd = this.handleDragEnd.bind(this);
    }

    boxStyle = styled.div.attrs(props => ({
        style: {
            top: props.Coordinate.y,
            left: props.Coordinate.x,
            width: props.ClassBoxState.BoxWidth,
            height: (props.ClassBoxState.AttrQuantity + props.ClassBoxState.OperationQuantity + props.ClassBoxState.ResponsibilityQuantity + 1) * props.ClassBoxState.LineHeight
        }
    }))`
    box-shadow:1px 1px inset,-1px -1px inset;
    position:absolute;
    background-color:white;
    display:flex;
    flex-direction:column;
    min-width:150px;
    `

    pointerBlockDiv = styled.div`
    width:inherit;
    height:inherit;
    pointer-events:${(props) => { return props.ClassBoxState.MakeRelationshipMode ? 'none' : '' }};
    `

    relationshipBar_top = styled.div`
    position:absolute;
    background-color:darkgreen;
    width:inherit;
    height:5px;
    top:-5px;
    left:0px;
    display:${(props) => { return props.ClassBoxState.MakeRelationshipMode ? '' : 'none' }};
    `


    relationshipBar_bottom = styled.div`
    position:absolute;
    background-color:darkgreen;
    width:inherit;
    height:5px;
    bottom:-5px;
    left:0px;
    display:${(props) => { return props.ClassBoxState.MakeRelationshipMode ? '' : 'none' }};
    `


    relationshipBar_left = styled.div`
    position:absolute;
    background-color:darkgreen;
    width:5px;
    height:inherit;
    top:0px;
    left:-5px;
    display:${(props) => { return props.ClassBoxState.MakeRelationshipMode ? '' : 'none' }};
    `


    relationshipBar_right = styled.div`
    position:absolute;
    background-color:darkgreen;
    width:5px;
    height:inherit;
    top:0px;
    right:-5px;
    display:${(props) => { return props.ClassBoxState.MakeRelationshipMode ? '' : 'none' }};
    `

    componentWillReceiveProps(nextProps, nextState)
    {
        if (nextProps.WorkSpaceState.mode === 'RelationshipEditing')
        {
            this.setState({ MakeRelationshipMode: true });
        } else
        {
            this.setState({ MakeRelationshipMode: false });
        }
    }

    render()
    {
        return (
            <this.boxStyle ref="classboxRefTest" ClassBoxState={this.state} ClassBoxProps={this.props} Coordinate={this.Coordinate}>
                <this.pointerBlockDiv ClassBoxState={this.state}>
                    <ClassTitleBar ClassBoxState={this.state} ClassBoxProps={this.props} deleteMe={this.deleteThis.bind(this)} sendDrag={this.getDrag.bind(this)}></ClassTitleBar>
                    <ClassAttrBox state={this.state} updateClassBoxHeight={this.updateAttrHeight.bind(this)}></ClassAttrBox>
                    <ClassOperationBox state={this.state} updateClassBoxHeight={this.updateOperationHeight.bind(this)}></ClassOperationBox>
                </this.pointerBlockDiv>
                <this.relationshipBar_top ClassBoxState={this.state} onClick={(e) => { this.getRelationshipPoint('top', e.pageX, e.pageY) }} ></this.relationshipBar_top>
                <this.relationshipBar_bottom ClassBoxState={this.state} onClick={(e) => { this.getRelationshipPoint('bottom', e.pageX, e.pageY) }}></this.relationshipBar_bottom>
                <this.relationshipBar_left ClassBoxState={this.state} onClick={(e) => { this.getRelationshipPoint('left', e.pageX, e.pageY) }}></this.relationshipBar_left>
                <this.relationshipBar_right ClassBoxState={this.state} onClick={(e) => { this.getRelationshipPoint('right', e.pageX, e.pageY) }}></this.relationshipBar_right>
            </this.boxStyle>
        )
    }

    updateAttrHeight(height)
    {
        if (this.state.AttrQuantity !== height)
        {
            this.setState({ AttrQuantity: height });
            this.props.noticeParent('classboxChanged', null);
        }
    }
    updateOperationHeight(height)
    {
        if (this.state.OperationQuantity !== height)
        {
            this.setState({ OperationQuantity: height });
            this.props.noticeParent('classboxChanged', null);
        }
    }

    deleteThis()
    {
        this.props.deleteMe(this.props.mykey);
        this.props.deleteRelationship(this.props.mykey);
    }

    //detect outside click;
    componentDidMount()
    {
        document.addEventListener('mousedown', this.handleClickOutside, true);
        document.addEventListener('mousedown', this.handleDragStart, true);
        document.addEventListener('mousemove', this.handleDragging, true);
        document.addEventListener('mouseup', this.handleDragEnd, true);
    }

    componentWillUnmount()
    {
        document.removeEventListener('mousedown', this.handleClickOutside, true);
        document.removeEventListener('mousedown', this.handleDragStart, true);
        document.removeEventListener('mousemove', this.handleDragging, true);
        document.removeEventListener('mouseup', this.handleDragEnd, true);
    }

    handleClickOutside(event)
    {
        let domNode = ReactDom.findDOMNode(this);
        if (!domNode || !domNode.contains(event.target))
        {
            this.setState({ Selected: false });
        }
        event.stopPropagation();
    }

    handleDragStart(event)
    {
        let domNode = ReactDom.findDOMNode(this);
        if (!domNode || domNode.contains(event.target))
        {
            this.setState({ Selected: true });
            if (this.drag.status)
            {
                this.drag.prevX = event.clientX;
                this.drag.prevY = event.clientY;
            }
        }
        event.stopPropagation();
    }

    handleDragging(event)
    {
        if (this.drag.status === true)
        {
            let disX = event.clientX - this.drag.prevX;
            let disY = event.clientY - this.drag.prevY;
            this.Coordinate.x = Math.max(this.Coordinate.x + disX, 0);
            this.Coordinate.y = Math.max(this.Coordinate.y + disY, 0);
            this.drag.prevX = event.clientX;
            this.drag.prevY = event.clientY;
            this.props.noticeParent('classboxChanged', null);
        }
    }

    handleDragEnd(event)
    {
        this.drag.status = false;
        this.drag.prevX = this.drag.prevY = null;
    }

    getDrag(dragStatus)
    {
        this.drag.status = dragStatus
    }

    getRelationshipPoint(side, clickX, clickY)
    {
        let percentage = null;
        let relateX = clickX - this.Coordinate.x;
        let relateY = clickY - this.Coordinate.y - 30;
        if (side === 'top' || side === 'bottom')
        {
            percentage = relateX / this.state.BoxWidth;
        }
        else
        {
            percentage = relateY / ((this.state.AttrQuantity + this.state.OperationQuantity + this.state.ResponsibilityQuantity + 1) * this.state.LineHeight);
        }
        this.props.noticeParent('RelationshipPointClicked', { 'side': side, 'percentage': percentage, 'classbox': this });
    }
}