import React from 'react'
import ReactDom from 'react-dom'
import styled from 'styled-components'
import OperationTitleBar from './OperationTitleBar/OperationTitleBar'
import OperationItem from './OperationItem/OperationItem'

export default class ClassOperationBox extends React.Component
{
    constructor(props)
    {
        super(props);

        this.operationContainer = new OperationContainer(this);

        this.OperationBoxShell = styled.div`
        position:relative;
        width:100%;
        height:${(props) => { return (props.state.classBoxState.LineHeight * (1 + props.length * 0.75)).toString().concat('px') }};
        left:0px;
        box-shadow:1px 1px black inset,-1px -1px black inset;
        `

        this.state = {
            classBoxState: this.props.state,
            selectedOperationItem: -1,
        }

        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState)
    {
        if (nextState !== this.state)
        {
            return true;
        } else
        {
            return false;
        }
    }

    componentWillUpdate()
    {
        this.props.updateClassBoxHeight(this.operationContainer.getSize() * 0.75 + 1);
    }

    render()
    {
        return (
            <this.OperationBoxShell state={this.state} length={this.operationContainer.getSize()}>
                <OperationTitleBar
                    state={this.state}
                    addItem={this.operationContainer.addItem.bind(this.operationContainer)}
                    deleteItem={this.operationContainer.deleteItem.bind(this.operationContainer)} >
                </OperationTitleBar>
                {this.operationContainer.getAllItems().map((value, index) =>
                {
                    return <OperationItem
                        key={value.getId()}
                        nodeInfo={value}
                        state={this.state}
                        id={value.getId()}
                        setFocus={this.getSelectedOperationItem.bind(this)}
                        updateItem={this.updateItem.bind(this)}>
                    </OperationItem>
                }, this)}
            </this.OperationBoxShell>
        )
    }

    // Item operations

    updateItem(id, name, type, accessibility, parameter, status)
    {
        this.operationContainer.updateItem(id, (tmp) =>
        {
            if (status !== 'modifing')
            {
                tmp.setName(name);
                tmp.setAccessibility(accessibility);
                tmp.setTypeName(type);
                tmp.setParameter(parameter);
            }
            tmp.setStatus(status);
        })
    }

    // callback function for child select setting
    getSelectedOperationItem(id)
    {
        this.setState({ selectedOperationItem: id });
    }

    //detect outside click;
    componentDidMount()
    {
        document.addEventListener('mousedown', this.handleClickOutside, true);
    }

    componentWillUnmount()
    {
        document.removeEventListener('mousedown', this.handleClickOutside, true);
    }

    handleClickOutside(event)
    {
        let domNode = ReactDom.findDOMNode(this);
        if (!domNode || !domNode.contains(event.target))
        {
            this.setState({ selectedOperationItem: -1 });
        }
    }
}

class OperationContainer
{
    constructor(owner)
    {
        this.container = new Map();
        this.counter = 0;
        this.owner = owner;
    }

    addItem()
    {
        this.container.set(this.counter, new OperationNode(this.counter));
        this.counter++;
        this.owner.forceUpdate();
    }

    deleteItem(id)
    {
        this.container.delete(id);
        this.owner.forceUpdate();
    }

    getItem(id)
    {
        if (this.container.has(id))
        {
            return this.container.get(id);
        } else
        {
            console.log('id not found in AttrContainer');
            return null;
        }
    }

    getAllItems()
    {
        let tmp = [];
        for (let value of this.container.values())
        {
            tmp.push(value);
        }
        return tmp;
    }

    updateItem(id, updateMethod)
    {
        let tmp = this.getItem(id);
        let result = updateMethod(tmp);
        if (typeof (result) !== 'undefined')
            this.container.set(id, result);
        this.owner.forceUpdate();
    }

    getSize()
    {
        return this.container.size;
    }
}


class OperationNode
{
    constructor(id)
    {
        this.id = id;
        this.typename = null;
        this.name = null;
        this.accessibility = null;
        this.parameter = null;
        this.status = "init";
    }
    getId() { return this.id }

    getName() { return this.name }

    setName(name)
    {
        if (name == null || typeof name != 'string')
        {
            this.name = "OperationName";
        } else
        {
            this.name = name;
        }
    }

    getAccessibility() { return this.accessibility }

    setAccessibility(accessibility)
    {
        switch (accessibility)
        {
            case "Public": { this.accessibility = accessibility; break; }
            case "Private": { this.accessibility = accessibility; break; }
            case "Protected": { this.accessibility = accessibility; break; }
            default: this.accessibility = "Public"; break;
        }
    }

    getTypeName() { return this.typename }

    setTypeName(typename)
    {
        if (typename == null || typeof (typename) != "string")
        {
            this.typename = "any";
        } else
        {
            this.typename = typename;
        }
    }

    getStatus() { return this.status };

    setStatus(status)
    {
        if (typeof (status) !== 'string')
        {
            this.status = "normal";
        } else
        {
            this.status = status;
        }
    }

    getParameter() { return this.parameter; }

    setParameter(parameter)
    {
        if (typeof (parameter) === 'string')
        {
            this.parameter = parameter;
        } else
        {
            this.parameter = '';
        }
    }
}