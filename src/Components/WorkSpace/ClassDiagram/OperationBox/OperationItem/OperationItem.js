import React from 'react'
import styled from 'styled-components'

export default class OperationItem extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            nodeInfo: this.props.nodeInfo,
            operationBoxState: this.props.state,
            height: this.props.state.classBoxState.LineHeight * 0.75,
            marginSize: 2,
            isSelected: false,

            editingAccessibility: null,
            editingName: null,
            editingType: null,
            editingParameter: null,
        }

        this.normalClickCount = 0;

        this.ShellStyle = styled.div`
        position:relative;
        top:0px;
        left:0px;
        width:100%;
        height:${(props) => { return (props.state.height).toString().concat('px') }};
        display:flex;
        align-items:center;
        background-color:${props => { if (this.state.isSelected) { return "lightgrey" } else { return "transparent" } }};
        `

        this.AccessibilityStyle = styled.select`
            height:calc(100% - 4px);
            width:${(props) => { return (props.state.height * 1.5).toString().concat('px') }};
            margin-left:${(props) => { return props.state.marginSize.toString().concat('px') }};
            font-size:${props => { return (props.state.height * 0.5).toString().concat('px') }};
            `;

        this.AccessibilitySelectStyle = styled.option`
            `;

        this.NameStyle = styled.input`
            height:calc(100% - 8px);
            padding:0px;
            width:40%;
            margin-left:${(props) => { return props.state.marginSize.toString().concat('px') }};
            font-size:${props => { return (props.state.height * 0.5).toString().concat('px') }};
            `;

        this.TypeNameStyle = styled.input`
            height:calc(100% - 8px);
            padding:0px;
            width:20%;
            margin-left:${(props) => { return props.state.marginSize.toString().concat('px') }};
            margin-right:${(props) => { return props.state.marginSize.toString().concat('px') }};
            font-size:${props => { return (props.state.height * 0.5).toString().concat('px') }};
            `;

        this.SubmitButton = styled.button`
            height:calc(100% - 4px);
            width:${(props) => { return (props.state.height * 1).toString().concat('px') }};
            margin-right:${(props) => { return props.state.marginSize.toString().concat('px') }};
            user-select:none;
            padding:0px;
            font-size:${props => { return (props.state.height * 0.5).toString().concat('px') }};
            `

        this.ParameterStyle = styled.input`
            height:calc(100% - 8px);
            padding:0px;
            width:40%;
            margin-left:${(props) => { return props.state.marginSize.toString().concat('px') }};
            font-size:${props => { return (props.state.height * 0.5).toString().concat('px') }};
        `

        this.DisplayMode = styled.p`
            position:relative;
            margin:0px;
            margin-left:${(props) => { return (props.state.marginSize * 4).toString().concat('px') }};
            font-size:${props => { return (props.state.height * 0.5).toString().concat('px') }};
            `
    }

    componentWillReceiveProps(nextProps, nextState)
    {
        if (nextProps.state.selectedOperationItem === nextProps.id)
        {
            if (this.state.isSelected === false)
            {
                this.setState({ isSelected: true });
            }
        } else
        {
            if (this.state.isSelected === true)
            {
                this.setState({ isSelected: false });
            }
        }
    }

    componentWillUpdate()
    {
    }

    normalClick()
    {
        this.props.setFocus(this.props.id);
        this.normalClickCount += 1;
        setTimeout((() =>
        {
            switch (this.normalClickCount)
            {
                default: break;
                case 0: break;
                case 1: break;
                case 2: this.props.updateItem(this.props.id, 0, 0, 0, 0, 'modifing');
            }
            this.normalClickCount = 0;
        }), 200);
    }

    submitClick()
    {
        this.props.updateItem(this.props.id, this.state.editingName, this.state.editingType, this.state.editingAccessibility, this.state.editingParameter, 'normal');
    }

    render()
    {
        if (this.props.nodeInfo.status === "modifing" || this.props.nodeInfo.status === "init")
        {
            var name, type, accessibility, parameter;
            if (this.props.nodeInfo.status === "modifing")
            {
                name = this.props.nodeInfo.name;
                type = this.props.nodeInfo.typename;
                accessibility = this.props.nodeInfo.accessibility;
                parameter = this.props.nodeInfo.parameter
            } else
            {
                name = type = accessibility = parameter = "";
            }
            return (
                <this.ShellStyle state={this.state} props={this.props}>
                    <this.AccessibilityStyle state={this.state} props={this.props}
                        onChange={((e) => { this.setState({ editingAccessibility: e.target.value }) })} defaultValue={accessibility}>
                        <this.AccessibilitySelectStyle value="Public" >+:Public</this.AccessibilitySelectStyle>
                        <this.AccessibilitySelectStyle value="Private" >-:Private</this.AccessibilitySelectStyle>
                        <this.AccessibilitySelectStyle value="Protected" >#:Protected</this.AccessibilitySelectStyle>
                    </this.AccessibilityStyle>
                    <this.NameStyle state={this.state} props={this.props} placeholder="Name" defaultValue={name}
                        onChange={((e) => { this.setState({ editingName: e.target.value }) })}></this.NameStyle>
                    <this.ParameterStyle state={this.state} props={this.props} placeholder="Parameter" defaultValue={parameter}
                        onChange={((e) => { this.setState({ editingParameter: e.target.value }) })}></this.ParameterStyle>
                    <this.TypeNameStyle state={this.state} props={this.props} placeholder="Type" defaultValue={type}
                        onChange={((e) => { this.setState({ editingType: e.target.value }) })}></this.TypeNameStyle>
                    <this.SubmitButton state={this.state} props={this.props} onClick={this.submitClick.bind(this)}>>></this.SubmitButton>
                </this.ShellStyle>
            )
        } else
        {
            let accessSymbol = null;
            switch (this.props.nodeInfo.accessibility)
            {
                case "Private": accessSymbol = '-'; break;
                case "Public": accessSymbol = '+'; break;
                case "Protected": accessSymbol = '#'; break;
                default: accessSymbol = '+'; break;
            }
            let str = accessSymbol + " " + this.props.nodeInfo.name + "(" + this.props.nodeInfo.parameter + ') : ' + this.props.nodeInfo.typename;
            return (
                <this.ShellStyle state={this.state} props={this.props} onClick={this.normalClick.bind(this)}>
                    <this.DisplayMode state={this.state} props={this.props}>{str}</this.DisplayMode>
                </this.ShellStyle>
            )
        }
    }
}