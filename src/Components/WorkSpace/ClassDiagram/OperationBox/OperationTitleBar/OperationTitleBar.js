import React from 'react'
import styled from 'styled-components'

export default class OperationTitleBar extends React.Component
{

    constructor(props)
    {
        super(props);
        this.state = {
            operationBoxState: this.props.state,
        }
        this.TitleBar = styled.div`
        position:relative;
        top:0px;
        left:0px;
        user-select:none;
        width:100%;
        height:${props => { return props.state.operationBoxState.classBoxState.LineHeight.toString().concat('px') }};
        display:flex;
        `
        this.OperationStr = styled.p`
        margin:0px;
        top:0px;
        left:0px;
        user-select:none;
        height:100%;
        font-size:${props => { return (props.state.operationBoxState.classBoxState.LineHeight / 2).toString().concat('px') }};
        display:flex;
        justify-content:left;
        align-items:center;
        padding-left:10px;
        margin-right:auto;
        `

        this.OperationAddButton = styled.button`
        margin:4px 4px 4px 4px;
        user-select:none;
        width:${props => { return (props.state.operationBoxState.classBoxState.LineHeight-8).toString().concat('px') }};
        padding:0px;
        `

        this.OperationDeleteButton = styled.button`
        user-select:none;
        margin:4px 4px 4px 4px;
        width:${props => { return (props.state.operationBoxState.classBoxState.LineHeight-8).toString().concat('px') }};
        `

    }

    render()
    {
        return (
            <this.TitleBar state={this.state}>
                <this.OperationStr state={this.state}>Operations</this.OperationStr>
                <this.OperationAddButton state={this.state} onClick={this.props.addItem}>+</this.OperationAddButton>
                <this.OperationDeleteButton state={this.state} onClick={(e) => { this.props.deleteItem(this.props.state.selectedOperationItem) }}>-</this.OperationDeleteButton>
            </this.TitleBar>
        )
    }

}