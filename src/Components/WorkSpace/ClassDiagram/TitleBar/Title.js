import React from 'react'
import styled from 'styled-components'
import ReactDom from 'react-dom'

export default class ClassTitleBar extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            titleName: "",
            editingTitle: null,
            mode: 'init',
        }

        this.titleStyle = styled.div`
        height:${(props) => { return (props.thisProps.ClassBoxState.LineHeight.toString().concat("px")) }};
        width:inherit;
        display:flex;
        justify-content:center;
        align-items:center;
        box-shadow:1px 1px black inset,-1px -1px black inset;
        user-select:none;
        background-color:${(props) => { return (props.thisProps.ClassBoxState.Selected ? 'lightblue' : 'white') }};
        `

        this.titleStrStyle = styled.p`
        margin:4px auto 4px auto;
        width:100%;
        height:calc(100% - 4px);
        display:flex;
        justify-content:center;
        align-items:center;
        font-size:${(props) => { return ((props.thisProps.ClassBoxState.LineHeight / 2).toString().concat("px")) }};
        `

        this.deleteButton = styled.button`
        margin:4px 4px 4px 4px;
        user-select:none;
        width:${props => { return ((props.thisProps.ClassBoxState.LineHeight - 8).toString().concat('px')) }};
        height:${props => { return ((props.thisProps.ClassBoxState.LineHeight - 8).toString().concat('px')) }};
        `

        this.submitButton = this.deleteButton;

        this.titleInput = styled.input`
        margin:4px auto 4px auto;
        width:80%;
        `

        this.handleDragStart=this.handleDragStart.bind(this);
    }

    render()
    {
        if (this.state.mode === 'init' || this.state.mode === 'modifing')
        {
            return (
                <this.titleStyle thisState={this.state} thisProps={this.props}>
                    <this.titleInput thisState={this.state} thisProps={this.props}
                        onChange={(e) => { this.setState({ editingTitle: e.target.value }) }}
                        placeholder='ClassName'
                        defaultValue={this.state.titleName}></this.titleInput>
                    <this.submitButton thisState={this.state} thisProps={this.props} onClick={() => this.setState({ titleName: this.state.editingTitle, mode: 'normal' })}>></this.submitButton>
                </this.titleStyle>
            )
        } else
        {
            return (
                <this.titleStyle thisState={this.state} thisProps={this.props}>
                    <this.titleStrStyle thisState={this.state} thisProps={this.props} onDoubleClick={
                        (e) => { if (this.state.mode !== 'normal') { return } else { this.setState({ mode: 'modifing' }) } }}>{this.state.titleName}</this.titleStrStyle>
                    <this.deleteButton thisState={this.state} thisProps={this.props} onClick={() => { this.props.deleteMe() }}>×</this.deleteButton>
                </this.titleStyle>
            )
        }
    }

    componentDidMount()
    {
        document.addEventListener('mousedown', this.handleDragStart, true);
    }

    componentWillUnmount()
    {
        document.removeEventListener('mousedown', this.handleDragStart, true);
    }


    handleDragStart(event)
    {
        let domNode = ReactDom.findDOMNode(this);
        if (!domNode || domNode.contains(event.target))
        {
            this.props.sendDrag(true)
        }
        event.stopPropagation();
    }

}