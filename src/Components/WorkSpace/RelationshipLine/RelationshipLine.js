import React from 'react'
import styled from 'styled-components'
import RelationshipSign from './RelationshipSign'

export default class RelationshipLine extends React.Component
{

    constructor(props)
    {
        super(props);
        this.state = {
            classbox1: this.props.setting.relationClass1.classbox,
            classbox2: this.props.setting.relationClass2.classbox,
            editingMode: true,
            currentRelationship1: 'Association',
            currentRelationship2: 'Association',
        }

        this.location = {
            point1: { x: null, y: null },
            point2: { x: null, y: null },
            coordinate: { top: null, left: null },
            angle: null,
            length: null,
        }

        this.LineStyle = styled.div.attrs(props => ({
            style: {
                top: props.location.coordinate.top,
                left: props.location.coordinate.left,
                width: props.location.length,
                transform: 'rotate(' + props.location.angle + 'deg)'
            },
        }))`
            height:0px;
            position:absolute;
            border-top:1px solid;
        `

        this.editing1Style = styled.select`
        position:absolute;
        top:-8px;
        left:10px;
        height:16px;
        width:40px;
        `
        this.editing2Style = styled.select`
        position:absolute;
        top:-8px;
        right:10px;
        height:16px;
        width:40px;
        `

        this.confirmButton = styled.button`
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        height:16px;
        width:16px;
        top:-8px;
        left:calc(50% - 9px);
        padding:0px;
        `

        this.deletButton = styled.button`
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        height:16px;
        width:16px;
        top:-8px;
        left:calc(50% + 9px);
        padding:0px;
        `
    }

    render()
    {
        let classbox1Coordinate = this.calculateCoordinate(this.props.setting.relationClass1);
        let classbox2Coordinate = this.calculateCoordinate(this.props.setting.relationClass2);
        this.location.point1.x = classbox1Coordinate.x;
        this.location.point1.y = classbox1Coordinate.y;
        this.location.point2.x = classbox2Coordinate.x;
        this.location.point2.y = classbox2Coordinate.y;

        this.location.length = Math.sqrt(Math.pow(this.location.point1.x - this.location.point2.x, 2) + Math.pow(this.location.point1.y - this.location.point2.y, 2))

        this.location.coordinate.top = (this.location.point1.y + this.location.point2.y) / 2;
        this.location.coordinate.left = (this.location.point1.x + this.location.point2.x) / 2 - this.location.length / 2;

        this.location.angle = (Math.asin((this.location.point1.y - this.location.point2.y) / this.location.length)) * 180 / Math.PI;
        if (this.location.point1.x < this.location.point2.x)
        {
            this.location.angle = 180 - this.location.angle;
        }
        if (this.state.editingMode)
        {
            return (
                <this.LineStyle location={this.location} editingMode={this.state.editingMode}>
                    <this.editing1Style defaultValue={this.state.currentRelationship1} onChange={(e) => { this.setState({ currentRelationship1: e.target.value }) }} style={{ transform: 'rotate(' + (-this.location.angle) + 'deg)' }}>
                        <option value='Association'> Association</option>
                        <option value='Inheritance'>Inheritance</option>
                        <option value='Implementation'>Implementation</option>
                        <option value='Dependency'>Dependency</option>
                        <option value='Aggregation'>Aggregation</option>
                        <option value='Composition'>Composition</option>
                    </this.editing1Style>
                    <this.confirmButton onClick={(() => { this.setState({ editingMode: false }) })} style={{ transform: 'rotate(' + (-this.location.angle) + 'deg)' }}>√</this.confirmButton>
                    <this.deletButton onClick={(() => { this.props.deleteMe(this.props.setting.key) })} style={{ transform: 'rotate(' + (-this.location.angle) + 'deg)' }}>×</this.deletButton>
                    <this.editing2Style defaultValue={this.state.currentRelationship2} onChange={(e) => { this.setState({ currentRelationship2: e.target.value }) }} style={{ transform: 'rotate(' + (-this.location.angle) + 'deg)' }}>
                        <option value='Association'> Association</option>
                        <option value='Inheritance'>Inheritance</option>
                        <option value='Implementation'>Implementation</option>
                        <option value='Dependency'>Dependency</option>
                        <option value='Aggregation'>Aggregation</option>
                        <option value='Composition'>Composition</option>
                    </this.editing2Style>
                    <RelationshipSign isHead={true} type={this.state.currentRelationship1}></RelationshipSign>
                    <RelationshipSign isHead={false} type={this.state.currentRelationship2}></RelationshipSign>
                </this.LineStyle >
            )
        } else
        {
            return (
                <this.LineStyle location={this.location} editingMode={this.state.editingMode} onDoubleClick={(() => { this.setState({ editingMode: true }) })}>
                    <RelationshipSign isHead={true} type={this.state.currentRelationship1}></RelationshipSign>
                    <RelationshipSign isHead={false} type={this.state.currentRelationship2}></RelationshipSign>
                </this.LineStyle>
            )
        }
    }

    calculateCoordinate(classboxInfo)
    {
        let classboxCoordinate = classboxInfo.classbox.Coordinate;
        let classboxState = classboxInfo.classbox.state;
        let height = (classboxState.AttrQuantity + classboxState.OperationQuantity + classboxState.ResponsibilityQuantity + 1) * classboxState.LineHeight
        let coordinate = { x: null, y: null };
        switch (classboxInfo.side)
        {
            case 'top':
                coordinate.x = classboxCoordinate.x + classboxInfo.percentage * classboxState.BoxWidth;
                coordinate.y = classboxCoordinate.y;
                break;
            case 'bottom':
                coordinate.x = classboxCoordinate.x + classboxInfo.percentage * classboxState.BoxWidth;
                coordinate.y = classboxCoordinate.y + height;
                break;
            case 'left':
                coordinate.x = classboxCoordinate.x;
                coordinate.y = classboxCoordinate.y + classboxInfo.percentage * height;
                break;
            case 'right':
                coordinate.x = classboxCoordinate.x + classboxState.BoxWidth;
                coordinate.y = classboxCoordinate.y + classboxInfo.percentage * height;
                break;
            default: break;
        }
        return coordinate;
    }
}