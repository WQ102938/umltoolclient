import React from 'react'
import styled from 'styled-components'

export default class RelationshipSign extends React.Component
{
    // constructor(props)
    // {
    //     super(props);//type
    // }

    InheritanceStyle = styled.div`
    position:absolute;
    top:-8px;
    width:0px;
    height:0px;
    left:${props => { return props.isHead ? '0px' : 'unset' }};
    right:${props => { return props.isHead ? 'unset' : '0px' }};
    transform:${props => { return props.isHead ? 'unset' : 'rotate(180deg)' }};
    border-left:0px;
    border-right:16px solid white;
    border-top:8px solid transparent;
    border-bottom:8px solid transparent;
    `

    InheritanceStyleBottom = styled.div`
    position:absolute;
    top:-10px;
    width:0px;
    height:0px;
    left:${props => { return props.isHead ? '-2px' : 'unset' }};
    right:${props => { return props.isHead ? 'unset' : '-2px' }};
    transform:${props => { return props.isHead ? 'unset' : 'rotate(180deg)' }};
    border-left:0px;
    border-right:20px solid black;
    border-top:10px solid transparent;
    border-bottom:10px solid transparent;
    `

    ImplementationStyle = styled.div`
    position:absolute;
    top:-8px;
    width:0px;
    height:0px;
    left:${props => { return props.isHead ? '-2px' : 'unset' }};
    right:${props => { return props.isHead ? 'unset' : '-2px' }};
    transform:${props => { return props.isHead ? 'unset' : 'rotate(180deg)' }};
    border-left:0px;
    border-right:16px solid black;
    border-top:8px solid transparent;
    border-bottom:8px solid transparent;
    `

    DependencyStyle = styled.div`
    position:absolute;
    top:${(props) => { return props.first ? '3px' : '-6px' }};
    width:18px;
    height:0px;
    left:${props => { return props.isHead ? '0px' : 'unset' }};
    right:${props => { return props.isHead ? 'unset' : '0px' }};
    transform:${props =>
        {
            if (props.isHead)
            {
                if (props.first)
                {
                    return 'rotate(30deg)'
                } else
                {
                    return 'rotate(-30deg)'
                }
            } else
            {
                if (props.first)
                {
                    return 'rotate(150deg)'
                } else
                {
                    return 'rotate(210deg)'
                }
            }
        }};
        border:1px solid black;
    `

    AggregationStyle = styled.div`
    position:absolute;
    top:-10px;
    width:16px;
    height:16px;
    left:${props => { return props.isHead ? '3px' : 'unset' }};
    right:${props => { return props.isHead ? 'unset' : '3px' }};
    transform:rotate(45deg);
    border:2px solid black;
    background-color:white;
    `

    CompositionStyle = styled.div`
    position:absolute;
    top:-10px;
    width:16px;
    height:16px;
    left:${props => { return props.isHead ? '3px' : 'unset' }};
    right:${props => { return props.isHead ? 'unset' : '3px' }};
    transform:rotate(45deg);
    background-color:black;
    border:2px solid black;
    `

    render()
    {
        switch (this.props.type)
        {
            case 'Association':
                return <></>;
            case 'Inheritance':
                return <>
                    <this.InheritanceStyleBottom isHead={this.props.isHead}></this.InheritanceStyleBottom>
                    <this.InheritanceStyle isHead={this.props.isHead}></this.InheritanceStyle>
                </>;
            case 'Implementation':
                return <>
                    <this.InheritanceStyleBottom isHead={this.props.isHead}></this.InheritanceStyleBottom>
                    <this.ImplementationStyle isHead={this.props.isHead}></this.ImplementationStyle>
                </>;
            case 'Dependency':
                return <div>
                    <this.DependencyStyle isHead={this.props.isHead} first={true}></this.DependencyStyle>
                    <this.DependencyStyle isHead={this.props.isHead} first={false}></this.DependencyStyle>
                </div>;
            case 'Aggregation':
                return <this.AggregationStyle isHead={this.props.isHead}></this.AggregationStyle>;
            case 'Composition':
                return <this.CompositionStyle isHead={this.props.isHead}></this.CompositionStyle>;
            default: return;
        }
    }
}