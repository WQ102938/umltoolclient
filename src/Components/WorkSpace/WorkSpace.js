import React from 'react'
import styled from 'styled-components'
import ClassBox from './ClassDiagram/ClassBox'
import RelationshipLine from './RelationshipLine/RelationshipLine'

export default class WorkSpace extends React.Component
{

    constructor(props)
    {
        super(props);

        this.state = {
            mode: 'normal',
            classboxMovingStatus: false,
        }

        this.ClassBoxContainer = new ClassBoxSet(this);
        this.RelationshipContainer = new RelationshipSet(this);

        this.relationshipSetting = { step: 0, classboxInfo1: null, classboxInfo2: null }

        this.ShellStyle = styled.div`
        width:100%;
        height:calc(100% - 30px);
        top:30px;
        position:absolute;
        `
    }

    componentWillReceiveProps(nextProps, nextState)
    {
        if (nextProps.globalState.AddClassBox.status === true)
        {
            this.ClassBoxContainer.addClassBox(nextProps.globalState.AddClassBox.coordinateX, nextProps.globalState.AddClassBox.coordinateY);
            this.props.noticeGlobal('ResetAddClassBox');
        }
        this.setState({ mode: nextProps.globalState.mode });
        this.forceUpdate();
    }

    render()
    {
        return (<this.ShellStyle>
            {this.ClassBoxContainer.getAllClassBox().map((value, index) =>
            {
                return <ClassBox key={value.key} mykey={value.key}
                    deleteMe={this.ClassBoxContainer.deleteClassBox.bind(this.ClassBoxContainer)}
                    deleteRelationship={this.RelationshipContainer.removeInvalidRelationship.bind(this.RelationshipContainer)}
                    noticeParent={this.classboxCallBack.bind(this)}
                    coordinateX={value.coordinateX}
                    coordinateY={value.coordinateY}
                    WorkSpaceState={this.state}></ClassBox>;
            })}
            {this.RelationshipContainer.getAllRelationship().map((value, index) =>
            {
                return <RelationshipLine
                    deleteMe={this.RelationshipContainer.deleteRelationship.bind(this.RelationshipContainer)}
                    key={value.key}
                    WorkSpaceState={this.state}
                    setting={value}
                ></RelationshipLine>
            })}
        </this.ShellStyle>)
    }

    componentDidUpdate()
    {

    }

    classboxCallBack(command, param)
    {
        if (command === 'RelationshipPointClicked')
        {
            if (this.state.mode === 'RelationshipEditing')
            {
                if (this.relationshipSetting.step === 0)
                {
                    this.relationshipSetting.classboxInfo1 = param;
                    this.relationshipSetting.step = 1;
                }
                if (this.relationshipSetting.step === 1)
                {
                    this.relationshipSetting.classboxInfo2 = param;
                    if (this.relationshipSetting.classboxInfo1.classbox.props.mykey !== this.relationshipSetting.classboxInfo2.classbox.props.mykey)
                    {
                        this.RelationshipContainer.addRelationship(this.relationshipSetting.classboxInfo1, this.relationshipSetting.classboxInfo2);
                        this.relationshipSetting = { step: 0, classboxInfo1: null, classboxInfo2: null };
                    }
                }
            } else
            {
                this.relationshipSetting = { step: 0, classboxInfo1: null, classboxInfo2: null };
            }
        }
        if (command === 'classboxChanged')
        {
            this.forceUpdate();
        }
    }
}

class ClassBoxSet
{

    constructor(owner)
    {
        this.classBoxContainer = new Map();
        this.owner = owner;
        this.classBoxAddCounter = 0;
    }

    addClassBox(coordinateX, coordinateY)
    {
        let key = 'ClassBox' + this.classBoxAddCounter.toString();
        this.classBoxContainer.set(key, {
            key: key, mykey: key,
            coordinateX: coordinateX,
            coordinateY: coordinateY,
        });
        this.classBoxAddCounter += 1;
        this.owner.forceUpdate();
    }

    deleteClassBox(ClassBoxKey)
    {
        this.classBoxContainer.delete(ClassBoxKey);
        this.owner.forceUpdate();
    }

    findClassBoxByKey(key)
    {
        if (this.classBoxContainer.has(key))
        {
            return this.classBoxContainer.get(key);
        } else
        {
            return null;
        }
    }

    getAllClassBox()
    {
        let tmpArray = [];
        for (let value of this.classBoxContainer.values())
        {
            tmpArray[tmpArray.length] = value;
        }
        return tmpArray;
    }
}


class RelationshipSet
{
    constructor(owner)
    {
        this.owner = owner;
        this.container = new Map();
        this.counter = 0;
    }

    addRelationship(relation1, relation2)
    {
        let key = 'Relationship' + this.counter.toString();
        this.container.set(key, {
            key: key,
            relationClass1: relation1,
            relationClass2: relation2,
            relationType1: 'unset',
            relationType2: 'unset',
        })
        this.counter += 1;
        this.owner.forceUpdate();
    }

    deleteRelationship(RelationshipKey)
    {
        this.container.delete(RelationshipKey);
        this.owner.forceUpdate();
    }

    removeInvalidRelationship(deletedClassKey)
    {
        for (let value of this.container.values())
        {
            if (value.relationClass1.classbox.props.mykey === deletedClassKey ||
                value.relationClass2.classbox.props.mykey === deletedClassKey)
            {
                this.deleteRelationship(value.key);
            }
        }
    }

    getAllRelationship()
    {
        let tmpArray = [];
        for (let value of this.container.values())
        {
            tmpArray[tmpArray.length] = value;
        }
        return tmpArray;
    }
}